# Ejercicios_complejos

El objetivo del proyecto es crear un programa que permita realizar operaciones básicas con números complejos.
Las operaciones que deberá de realizar es sumas, restas, multiplicaciones y divisiones para cualquier número complejo.

Para ello el programa se compondrá de una clase llamada "Complejos". Esta clase tendrá un método constructor integrado por dos atributos que son prederteminados para llamar a la clase complejos, es decir, para que se llame al funcionamiento de la clase se debe poner(Complejos(real,imaginario)). Siendo los parámetros cualquier número.

Se encuentran seis métodos de la clase Compelejos además del constructor que permitirán el desarrollo de las operaciones.Los métodos tendrán un párametro necesario para la ejecución de la operación; este atributo es un objeto que está integrado por un número complejo, lo que permite, asociar ambos objetos.

Por un lado, el método sumar que te permite realizar la suma de dos número complejos mediante el mecanismo de suma de cada término real de ambos números y viceversa con el imaginario. En el método de la resta es similar pero realizando la operación resta. 

El método multiplicar es necesario realizar unas líneas de código más para obtener el resultado adecuado. Primero se formará el número real compuesto por la multiplicación de los elementos reales de cada número y los elementos imaginarios con un signo menos delante debido al "i cuadrado". Posteriormnete su suma. Del mismo modo ocurre con los imaginarios, teniendo que multiplicar el elemento imaginario con el elemneto real en cada caso.
 
El método dividir es un código más complicado ya que para formar las operaciones del numerador se ha creados dos métodos nuevos: numerador_real y numerador_imaginario. Estos dos métodos de la clase permiten crear el numerador que posteriormente será dividido cada término por el denominador común; obtenido por medio de la suma de cada elemento del segundo número  al cuadrado.

El último método es el str que permite la reprensetación por pantalla del resultado teniendo un formato siempre de "x +/- yi" cambiando el signo depend*iendo del valor del número imaginario resultante es negativo o postivo.

Para terminar, se ha realizado la creación de prueba de casos que permite comprobar que el programa es correcto para ello se han realizado varias pruebas cuyo resultado son exitosos.