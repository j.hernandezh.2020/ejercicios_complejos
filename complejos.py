
#!/usr/bin/env python3
from fractions import Fraction

class Complejos():
    def __init__(self, a, b):
        self.real = a
        self.imaginario = b

    def sumar(self, c2):
        real = self.real + c2.real
        imagine = self.imaginario + c2.imaginario
        return Complejos(real,imagine)

    def restar(self, c2):
        real = self.real - c2.real
        imagine = self.imaginario - c2.imaginario
        return Complejos(real,imagine)

    def multiplicar(self, c2):
        part_r = self.real * c2.real
        part_r2 = -(self.imaginario * c2.imaginario)
        reales_m = part_r + part_r2
        part_i = self.imaginario * c2.real
        part_i2 = (self.real * c2.imaginario)
        imagine_m = part_i + part_i2
        return Complejos(reales_m, imagine_m)

    def numerador_real (self,c2):
        part_r = self.real * (c2.real)
        part_r2 = self.imaginario * (c2.imaginario)
        reales_m = part_r + part_r2
        return reales_m
    def numerador_imaginario (self,c2):
        part_i = self.imaginario * (c2.real)
        part_i2 = self.real * (-c2.imaginario)
        imagine_m = part_i + part_i2
        return imagine_m
    
    def dividir(self,c2):
        denominador = c2.real**2 + c2.imaginario**2
        real_d = self.numerador_real(c2)/denominador
        imagine_d = self.numerador_imaginario(c2)/denominador
        return Complejos(real_d,imagine_d)

    def __str__(self):
        if self.imaginario<0:
            return "{reall} {imag}i".format(reall=self.real, imag=self.imaginario)
        else:
            return "{reall} + {imag}i".format(reall=self.real, imag=self.imaginario)

