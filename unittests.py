#!/usr/bin/env python3

import unittest
from complejos import Complejos

class TestComplejos(unittest.TestCase):

    def test_construir_real(self):
        e1 = Complejos(6,3)
        self.assertEqual(e1.real, 6)

    def test_construir_imaginario(self):
        e1 = Complejos(6,3)
        self.assertEqual(e1.imaginario, 3)

    def test_sumar(self):
        c1 = Complejos(1,2)
        c2 = Complejos(3,4)

        suma = c1.sumar(c2)

        self.assertEqual(suma.real, 4)
        self.assertEqual(suma.imaginario, 6)

    def test_restar(self):
        
        c1 = Complejos(6,1)
        c2 = Complejos(8,4)

        restar = c1.restar(c2)

        self.assertEqual(restar.real, -2)
        self.assertEqual(restar.imaginario, -3)

    def test_multiplicar(self):
        c1 = Complejos(1,1)
        c2 = Complejos(1,1)

        multiplicar = c1.multiplicar(c2)

        self.assertEqual(multiplicar.real, 0)
        self.assertEqual(multiplicar.imaginario, 2)
        

    def test_dividir(self):
        c1 = Complejos(3,1)
        c2 = Complejos(2,1)

        dividir = c1.dividir(c2)

        self.assertEqual(dividir.real, 1.4)
        self.assertEqual(dividir.imaginario, -0.2)
       

if __name__ == "__main__":
    unittest.main()